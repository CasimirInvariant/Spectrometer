
from PyQt5 import QtGui
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QDialog, QInputDialog
from peakfinderUi import Ui_PeakFinderDialog
from PyQt5.QtCore import Qt
import math
import numpy as np

array_size = 3648


class PeakFinderDialog(QDialog):

    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self.setModal(False)
        self.setWindowFlags(self.windowFlags() & (~Qt.WindowContextHelpButtonHint) | Qt.MSWindowsFixedSizeDialogHint)
        self.ui = Ui_PeakFinderDialog()
        self.ui.setupUi(self)

        self.ui.spinBoxSGOrder.valueChanged.connect(lambda: changed(True))
        self.ui.spinBoxSGWindow.valueChanged.connect(lambda: changed(False))

        def changed(is_order):
            nonlocal self
            sw = self.ui.spinBoxSGWindow.value()
            so = self.ui.spinBoxSGOrder.value()
            if sw % 2 == 0:
                sw = sw - 1
            if so >= sw:
                if is_order:
                    sw = sw + 2
                else:
                    so = so - 1
            self.ui.spinBoxSGWindow.setValue(sw)
            self.ui.spinBoxSGOrder.setValue(so)

        self.ui.pushButtonFind.clicked.connect(self.find_peak)
        self.ui.pushButtonSave.clicked.connect(self.save_peak)
        self.show()

    def reject(self):
        self.parent().data_find, self.parent().peaks = None, None
        self.parent().plot()
        self.parent().enable_all("__find_peak")
        super().reject()

    def find_peak(self):
        par = self.parent()

        # preprocess
        window = self.ui.spinBoxSGWindow.value()
        order = self.ui.spinBoxSGOrder.value()
        sub_data = np.array(par.data[0]) - (np.array(par.baseline) if par.is_sub_baseline_checked() else 0)

        from scipy.signal import savgol_filter
        par.data_find = sub_data if window == 1 else savgol_filter(sub_data, window_length=window, polyorder=order)

        # get global height threshold
        df_max, df_min = max(par.data_find), min(par.data_find)
        gh = self.ui.spinBoxMinHeight.value()
        height_threshold = (df_max - df_min) * gh / 100 + df_min

        lw, lh = self.ui.spinBoxLFWindow.value(), self.ui.spinBoxLFMinHeight.value()

        par.peaks = []
        st, window = 0, []  # local window start
        for j in range(1, array_size - 1):
            if not (j < lw / 2 or j >= array_size - math.ceil(lw / 2)):  # not in head/tail window
                st = st + 1
            window = par.data_find[st:st + lw]
            if par.data_find[j] >= height_threshold:  # passes global height threshold
                if par.data_find[j] > par.data_find[j - 1] and par.data_find[j] > par.data_find[j + 1]:  # is peak
                    if par.data_find[j] > (max(window) - min(window)) * lh / 100 + min(window):
                        par.peaks.append(j)

        if len(par.peaks) == 0:
            self.ui.pushButtonSave.setEnabled(False)
        else:
            self.ui.pushButtonSave.setEnabled(True)
        par.plot()

    def save_peak(self):
        par = self.parent()
        dlg = QInputDialog()
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/acq-icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        dlg.setWindowIcon(icon)
        dlg.setWindowFlags(dlg.windowFlags() & (~Qt.WindowContextHelpButtonHint) | Qt.MSWindowsFixedSizeDialogHint)
        dlg.setInputMode(QInputDialog.TextInput)
        dlg.setFont(QFont("Segoe UI", 9))
        dlg.setWindowTitle("Copy Peaks")
        dlg.setLabelText("Peak positions:")
        dlg.setOptions(dlg.options() | QInputDialog.UsePlainTextEditForTextInput | QInputDialog.NoButtons)
        dlg.setTextValue("\n".join(list(map(
            lambda e: "%.2f" % (par.x_position(e)), par.peaks
        ))))
        dlg.resize(180, self.height())
        dlg.exec_()
