
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon

import matplotlib
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg
from matplotlib.figure import Figure

import numpy as np

array_size = 3648


class NavigationToolbar(NavigationToolbar2QT):

    toolitems = [t for t in NavigationToolbar2QT.toolitems if t[0] in ("Home", "Pan", "Zoom", "Save")]


class PlotWidget(QtWidgets.QWidget):

    def __init__(self, parent):

        QtWidgets.QWidget.__init__(self, parent)

        # Creating QChartView
        matplotlib.rcParams['savefig.dpi'] = 750
        self.canvas = FigureCanvasQTAgg(Figure())
        self.ax = self.canvas.figure.subplots()
        self.labels = lambda: self.ax.get_xticklabels() + self.ax.get_yticklabels()
        self.canvas.figure.set_visible(False)

        # QWidget Layout
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.canvas)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setEnabled(False)

        self.toolbarActions = self.toolbar.actions()

        self.toolbar.removeAction(self.toolbarActions[-1])
        for action in self.toolbarActions:
            if action.text() == "Home":
                action.setIcon(QIcon(":/icons/icons/reset.png"))
                action.triggered.disconnect()
                action.triggered.connect(self.plot)
            elif action.text() == "Pan":
                action.setIcon(QIcon(":/icons/icons/pan.png"))
            elif action.text() == "Zoom":
                action.setIcon(QIcon(":/icons/icons/zoom.png"))
            elif action.text() == "Save":
                action.setIcon(QIcon(":/icons/icons/img.png"))
        self.layout.addWidget(self.toolbar)
        self.layout.setAlignment(self.toolbar, Qt.AlignHCenter)
        self.setLayout(self.layout)

    def update_units(self):

        self.setLayout(self.layout)
        self.canvas.figure.set_visible(True)
        self.ax.tick_params(direction="in", labelsize=10)
        par = self.parent().parent()
        [label.set_fontname("Arial") for label in self.labels()]
        fonts = {
            "fontsize": 12,
            "fontname": "Arial"
        }
        self.ax.set_ylabel("Intensity", fonts)
        if par.units == 0:
            self.ax.set_xlabel("Pixels", fonts)
        elif par.units == 1:
            self.ax.set_xlabel("Wavenumber / $\\mathregular{cm^{-1}}$", fonts)
        elif par.units == 2:
            self.ax.set_xlabel("Wavelength / nm", fonts)

        self.canvas.figure.tight_layout()
        self.ax.figure.canvas.draw()

    def plot(self):
        par = self.parent().parent()
        self.ax.clear()
        x = np.array([par.x_position(i) for i in range(array_size)])

        for i in range(len(par.data)):
            y = np.array(par.data[i])
            if par.is_sub_baseline_checked():
                y = y - np.array(par.baseline)
            if len(par.data) == 1:
                self.ax.plot(x, y, color="cornflowerblue")
            else:
                self.ax.plot(x, y, label=par.data_name[i])

        if len(par.data) != 1:
            self.ax.legend(prop={"family": "Arial", "size": 12})

        if par.ui.checkBoxFixAxisRange.isChecked():
            self.ax.set_ylim([par.yMin, par.yMax])

        if par.data_find is not None and par.peaks is not None:

            def to_precision(val, sig):
                s = '{:g}'.format(float('{:.{sig}g}'.format(val, sig=sig)))
                sig_ = len(s.lstrip("0.").replace(".", ""))
                if sig_ < sig:
                    s = s + ("" if s.find(".") >= 0 else ".") + ''.join(['0'] * (sig - sig_))
                return s

            self.ax.plot(x, par.data_find, color="limegreen")
            self.ax.scatter(list(map(par.x_position, par.peaks)), [par.data_find[j] for j in par.peaks],
                            marker="|", zorder=3, color="black", s=120, linewidths=1)
            for j in par.peaks:
                self.ax.text(par.x_position(j), par.data_find[j], to_precision(par.x_position(j), 6),
                             fontsize=9, fontname="Arial", clip_on=True)

        self.update_units()
