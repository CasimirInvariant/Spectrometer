# Spectrometer

Spectrometer is a program that accesses the
CCD provided by Peking University Comprehensive Physical Chemistry Lab and integrates basic
tasks of spectroscopy, including hand-made Raman, UV-Vis and others.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes.

### Requirements

- Python 3.7+
- pip

### Setting up Virtualenv

Download source code directly or use `git clone`. `cd `
into the directory and recover the venv in Windows console

```cmd
python3 -m venv venv
call venv\scripts\activate
pip install -r requirements.txt
```

and `run`.

### Running 

Start a Windows console and
```cmd
call venv\scripts\activate
run
```

Updating `*UI.py` files by `run update`. If you don't want to update
`*UI.py` files on every run, call `run run`.

### Building with PyInstaller

Start a Windows console and
```cmd
call venv\scripts\activate
run build
```

Into one file: `run buildone`, or with console window: `run debug`, `run debugone`.

The built files are in `dist` folder.

## License

This project is licensed under GPLv3.