@echo off

REM if argv is specified as any other command
REM it will work as update

if not "%1"=="run" (
pyrcc5 resources/resources.qrc -o resources_rc.py && ^
pyuic5 main.ui -o mainUi.py && ^
pyuic5 peakfinder.ui -o peakfinderUi.py && ^
pyuic5 calibration.ui -o calibrationUi.py
)

if "%1"=="debug" (
python -m PyInstaller spectrometer.py --add-data libusb0.dll;. --icon=spectrometer.ico
)

if "%1"=="debugone" (
python -m PyInstaller spectrometer.py -F --add-data libusb0.dll;. --icon=spectrometer.ico
)

if "%1"=="build" (
python -O -m PyInstaller spectrometer.py -w --add-data libusb0.dll;. --icon=spectrometer.ico
)

if "%1"=="buildone" (
python -O -m PyInstaller spectrometer.py -w -F --add-data libusb0.dll;. --icon=spectrometer.ico
)

if "%1"=="run" python spectrometer.py
if "%1"=="" python spectrometer.py
