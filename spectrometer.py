
import sys
import webbrowser

import usb
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMessageBox

import calibration
import mainUi
import peakfinder

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

array_size = 3648
integration_delay = 300


def set_enabled(self, enabled, lock_from):
    if self.enableLock is None or self.enableLock == lock_from:
        if enabled is True:
            assert self.enableLock == lock_from or self.enableLock is None
            self.enableLock = None
        else:
            assert lock_from is not None
            self.enableLock = lock_from
        self.setEnabled(enabled)


QtWidgets.QWidget.enableLock = None
QtWidgets.QAction.enableLock = None
QtWidgets.QWidget.setEnabled_ = set_enabled
QtWidgets.QAction.setEnabled_ = set_enabled


class MainWindow(QtWidgets.QMainWindow):

    resized = QtCore.pyqtSignal()

    def resizeEvent(self, event):
        self.resized.emit()
        return super().resizeEvent(event)

    def plot(self):
        self.ui.plotWidget.plot()

    def integration_time(self):
        time, unit = self.ui.spinBoxITTime.value(), self.ui.comboBoxITUnit.currentIndex()
        return time * (1 if unit == 0 else 1000)

    def accumulation(self):
        return self.ui.spinBoxAccumulation.value()

    def __plot_resize(self):
        if self.data is not None:
            self.plot()

    def __check_driver(self):

        def freezer(enabled):
            for i in self.deviceOperators:
                i.setEnabled_(enabled, "__check_driver")
            return enabled

        try:

            if self.device is None:
                self.device = usb.core.find()
                if self.device is None:
                    QMessageBox.critical(self, "Error", "No device found")
                    return freezer(False)

            # apply acquisition settings
            s = ["ms", "ss"]
            time, unit = self.ui.spinBoxITTime.value(), self.ui.comboBoxITUnit.currentIndex()
            self.device.write(1, "#IT:%03d" % time + s[unit])

        except (usb.core.USBError, NotImplementedError):
            QMessageBox.critical(self, "Error", "Device unplugged or missing driver")
            return freezer(False)
        return freezer(True)

    def __acq_read(self):
        arr = self.device.read(0x81, 10000)
        ret = []
        for i in range(32, 32 + array_size):
            ret.append(arr[2 * i] + arr[2 * i + 1] * 256)
        return ret

    def __acquisition(self, acc, rtd):

        # play safe
        if acc == 0:
            self.data = [[0] * array_size]

        self.ui.progressBar.setValue(int(acc / self.accumulation() * 100))
        timer = QtCore.QTimer()
        self.device.write(1, "#Onestep%")

        def effects(acc_, rtd_):
            nonlocal self
            re = self.__acq_read()
            # in rtd mode, self.data is always zeros; acc_ is always 0.
            self.data = [[(self.data[0][j] * acc_ + re[j]) / (acc_ + 1) for j in range(array_size)]]
            self.plot()
            timer.deleteLater()

            if rtd_ is True:

                if not self.ui.actionSpectrum_RTD.isChecked():  # termination signal detected
                    self.enable_all("__rtd")  # release all __rtd locks, including actionSpectrum_RTD
                    self.__update_data_acquired_status()  # release all __data_acquired locks
                else:
                    self.ui.progressBar.setValue(100)
                    self.__acquisition(acc=0, rtd=True)

            else:

                if self.ui.actionAcquisition.isChecked():  # no manual termination
                    if acc + 1 < self.accumulation():  # ongoing
                        self.ui.progressBar.setValue(int((acc_ + 1) / self.accumulation() * 100))
                        self.__acquisition(acc=acc_ + 1, rtd=False)
                        return
                    else:  # stopped
                        self.ui.actionAcquisition.setChecked(False)
                        # and in __acq(False): actionAcquisition.setEnabled_(False, "__acquisition")
                else:  # manual termination
                    QMessageBox.warning(self, "Warning", "Acquisition cancelled")

                # overall termination
                self.ui.progressBar.setValue(100)
                self.data_name = ["Current"]
                self.enable_all("__acquisition")  # release all __acquisition locks, including actionAcquisition
                self.__update_data_acquired_status()  # release all __data_acquired locks

        timer.singleShot(self.integration_time() + integration_delay, lambda: effects(acc, rtd))

    def __acq(self, checked):
        if checked:
            if self.__check_driver() is False:
                return
            self.__cancel_overlay()
            self.freeze_all("__acquisition", [self.ui.actionAcquisition])  # acquire __acquisition locks
            self.__acquisition(acc=0, rtd=False)
        else:
            self.ui.actionAcquisition.setEnabled_(False, "__acquisition")  # temp. lock Acq action

    def __rtd(self, checked):
        if checked:
            if self.__check_driver() is False:
                return
            self.__cancel_overlay()
            self.freeze_all("__rtd", [self.ui.actionSpectrum_RTD])  # acquire __rtd locks
            self.ui.groupBoxYAxisFixer.setEnabled_(True, "__rtd_exclusive")  # release exclusive lock to YAxisFixer
            self.ui.checkBoxFixAxisRange.setChecked(False)  # disable the sliders (on 1st time rtd ~ YAxisFixer enabled)
            self.__acquisition(acc=0, rtd=True)
        else:
            self.ui.actionSpectrum_RTD.setEnabled_(False, "__rtd")  # temporarily lock RTD action with __rtd
            self.ui.checkBoxFixAxisRange.setChecked(False)  # disable Y-Axis fixing
            self.ui.groupBoxYAxisFixer.setEnabled_(False, "__rtd_exclusive")  # acquire exclusive lock to YAxisFixer

    def __baseline_acquisition(self, acc=0):

        if acc == 0:
            self.data = [[0] * array_size]  # no need to initialize baseline; no rtd mode

        self.ui.progressBar.setValue(int(acc / self.accumulation() * 100))
        timer = QtCore.QTimer()
        self.device.write(1, "#Onestep%")

        def effects(acc_):
            nonlocal self

            re = self.__acq_read()
            self.data = [[(self.data[0][j] * acc_ + re[j]) / (acc_ + 1) for j in range(array_size)]]
            self.plot()
            timer.deleteLater()

            if self.ui.pushButtonBaselineAcq.isChecked():  # no manual termination
                if acc + 1 < self.accumulation():  # ongoing
                    self.ui.progressBar.setValue(int((acc_ + 1) / self.accumulation() * 100))
                    self.__baseline_acquisition(acc=acc_ + 1)
                    return
                else:  # stopped
                    self.ui.pushButtonBaselineAcq.setChecked(False)
                    # and in __b_acq(False): pushButtonBaselineAcq.setEnabled_(False, "__baseline_acquisition")
            else:  # manual termination
                QMessageBox.warning(self, "Warning", "Baseline acquisition cancelled")

            # overall termination
            self.ui.progressBar.setValue(100)
            self.__update_data_acquired_status()
            self.baseline = [i for i in self.data[0]]  # deepcopy VISUALIZED self.data to self.baseline AS-IS
            self.__accept_baseline()
            self.enable_all("__baseline_acquisition")

        timer.singleShot(self.integration_time() + integration_delay, lambda: effects(acc))

    def __b_acq(self, checked):
        if checked:
            if self.__check_driver() is False:
                return
            self.freeze_all("__baseline_acquisition", [self.ui.pushButtonBaselineAcq])  # acquire locks
            self.__baseline_acquisition(acc=0)
        else:
            self.ui.pushButtonBaselineAcq.setEnabled_(False, "__baseline_acquisition")  # temp. lock b acq action

    def freeze_all(self, call_from, excepts, allow_toolbar=False):
        excepts.extend(self.exemptOperators)
        # these locks are managed by __rtd_exclusive
        if allow_toolbar:
            excepts.append(self.ui.plotWidget.toolbar)
        else:
            self.plot()
            action_list = self.ui.plotWidget.toolbarActions
            if action_list[1].isChecked():
                self.ui.plotWidget.toolbar.pan()
            elif action_list[2].isChecked():
                self.ui.plotWidget.toolbar.zoom()
        for i in self.operatorList:
            if i not in excepts:
                i.setEnabled_(False, call_from)

    def enable_all(self, call_from):
        for i in self.operatorList:
            if i not in self.exemptOperators:  # these locks are managed by __rtd_exclusive
                i.setEnabled_(True, call_from)

    def __save(self, save_type):
        is_export = (save_type == "export")
        filename = QtWidgets.QFileDialog.getSaveFileName(self,
                                                         "Export to text" if is_export else "Save Datafile", "",
                                                         "Text (*.txt)" if is_export else "Data (*.dat)")
        if filename[0] == '':
            return
        else:
            try:
                with open(filename[0], "w") as f:
                    if is_export:
                        # EXPORT AS-IS
                        fl = self.is_sub_baseline_checked()
                        for i in range(array_size):
                            x = self.x_position(i)
                            y = self.data[0][i] - (self.baseline[i] if fl else 0)
                            f.write("%.2f" % x + " " + str(y) + "\n")
                    else:
                        for i in range(array_size):
                            f.write(str(self.baseline[i] if save_type == "baseline" else self.data[0][i]) + "\n")
            except (TypeError, ValueError, IOError):
                QMessageBox.critical(self, "Error", "Cannot write file. Please check again.")
                return

    def __load(self, _args):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Open Datafile", "", "Data (*.dat)")
        if filename[0] == '':
            return None
        else:
            try:
                with open(filename[0]) as f:
                    data = f.readlines()
                data = list(filter(lambda e: True if e == 0 else e, [float(x) for x in data]))  # 0 is OK
                if len(data) != array_size:
                    raise ValueError
            except (TypeError, ValueError, IOError):
                QMessageBox.critical(self, "Error", "Data file is corrupted. Please check again.")
                return None
            assert filename[0].endswith(".dat")
            return data, filename[0].split("/")[-1][:-4]
        pass

    def __load_data(self, _args):
        ret = self.__load(_args)
        if ret is not None:
            self.data = [ret[0]]
            self.data_name = [ret[1]]
            self.plot()
            self.__update_data_acquired_status()
            self.__cancel_overlay()
        else:
            return
        pass

    def __overlay(self, _args):
        ret = self.__load(_args)
        if ret is not None:
            operators = [self.ui.actionSave_As, self.ui.actionExport, self.ui.actionFind_Peak]
            for operator in operators:
                operator.setEnabled_(False, "__overlay")
            self.data.append(ret[0])
            self.data_name.append(ret[1])
            self.plot()

    def __cancel_overlay(self):
        operators = [self.ui.actionSave_As, self.ui.actionExport, self.ui.actionFind_Peak]
        for operator in operators:
            operator.setEnabled_(True, "__overlay")

    def __update_data_acquired_status(self, acquired=True):
        for operators in self.fileOperators:
            operators.setEnabled_(acquired, "__data_acquired")

    def __load_baseline(self, _unused):
        ret = self.__load(_unused)
        if ret is not None:
            if self.data is None:
                self.baseline = ret[0]
                self.__update_data_acquired_status()
                self.data = [[i for i in self.baseline]]  # deepcopy
            else:
                self.baseline = ret[0]
            print(self.baseline)
        else:
            return
        self.__accept_baseline()
        self.plot()

    def __accept_baseline(self):
        assert self.baseline is not None
        self.ui.checkBoxSubtractBaseline.setEnabled_(True, "__accept_baseline")
        self.ui.checkBoxSubtractBaseline.setChecked(False)
        self.ui.pushButtonSaveBaseline.setEnabled_(True, "__accept_baseline")

    def is_sub_baseline_checked(self):
        if self.ui.checkBoxSubtractBaseline.isChecked():
            assert self.baseline is not None
            return True
        return False

    def __calibration(self):
        a1, a2, laser, ok = calibration.CalibrationDialog.get_calibration_params(self, self.a1, self.a2, self.laser)
        if ok and (a1 != self.a1 or a2 != self.a2 or laser != self.laser):
            self.a1, self.a2, self.laser = a1, a2, laser
            if self.data is not None:
                self.plot()
        return ok  # 1 or 0

    def x_position(self, pixel):
        if self.units == 0:
            return pixel
        elif self.units == 1:
            return (1. / self.laser - 1. / (self.a1 * pixel + self.a2)) * 1e7  # cm^{-1}
        elif self.units == 2:
            return self.a1 * pixel + self.a2  # nm

    def __find_peak(self):
        self.freeze_all("__find_peak", [], allow_toolbar=True)
        peakfinder.PeakFinderDialog(self)

    def on_start(self):
        self.resized.connect(self.__plot_resize)

    def __init__(self):

        super(MainWindow, self).__init__()

        self.baseline, self.data = None, None
        self.device = None
        self.data_find, self.peaks = None, None
        self.data_name = None

        self.units = 0  # (0, 1, 2) = (pixel, wavenumber, wavelength)
        self.units_lock = False
        self.a1 = None
        self.a2 = None
        self.laser = 532

        # setting up handlers

        self.ui = mainUi.Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.actionOpen.triggered.connect(self.__load_data)
        self.ui.actionExit.triggered.connect(sys.exit)
        self.ui.pushButtonLoadBaseline.clicked.connect(self.__load_baseline)
        self.ui.checkBoxSubtractBaseline.stateChanged.connect(self.plot)
        self.ui.actionRecheck_Driver.triggered.connect(self.__check_driver)
        self.ui.actionSave_As.triggered.connect(lambda: self.__save("save"))
        self.ui.actionExport.triggered.connect(lambda: self.__save("export"))
        self.ui.pushButtonSaveBaseline.clicked.connect(lambda: self.__save("baseline"))
        self.ui.actionCalibration.triggered.connect(self.__calibration)
        self.ui.actionFind_Peak.triggered.connect(self.__find_peak)
        self.ui.actionAcquisition.toggled.connect(self.__acq)
        self.ui.actionSpectrum_RTD.toggled.connect(self.__rtd)
        self.ui.pushButtonBaselineAcq.toggled.connect(self.__b_acq)
        self.unitActions = [self.ui.actionUnitPixels, self.ui.actionUnitWavenumber, self.ui.actionUnitWavelength]
        self.ui.actionUnitPixels.toggled.connect(lambda checked: self.__unit_toggled(checked, 0))
        self.ui.actionUnitWavenumber.toggled.connect(lambda checked: self.__unit_toggled(checked, 1))
        self.ui.actionUnitWavelength.toggled.connect(lambda checked: self.__unit_toggled(checked, 2))
        site = "https://gitlab.com/CasimirInvariant/Spectrometer"
        self.ui.actionAbout.triggered.connect(lambda: webbrowser.open_new_tab(site))
        self.ui.actionOverlay.triggered.connect(self.__overlay)
        self.ui.checkBoxFixAxisRange.toggled.connect(self.__y_axis_fixing_toggled)
        self.ui.horizontalSliderYMin.valueChanged.connect(lambda: self.__y_axis_fixing_value_changed(True))
        self.ui.horizontalSliderYMax.valueChanged.connect(lambda: self.__y_axis_fixing_value_changed(False))
        self.yMin = self.ui.horizontalSliderYMin.minimum() * 1000
        self.yMax = self.ui.horizontalSliderYMax.maximum() * 1000
        self.ui.comboBoxITUnit.currentIndexChanged.connect(self.__integration_time_changed)
        self.ui.spinBoxITTime.valueChanged.connect(self.__integration_time_changed)

        action_list = [x for x in self.findChildren(QtWidgets.QAction) if x.objectName().startswith("action")]

        def fw(obj_types):
            nonlocal self
            return [x for x in sum((
                self.ui.centralwidget.findChildren(obj_type) for obj_type in obj_types
            ), []) if x.objectName() != ""]
        # sum(generator, []) better(?) than sum(list, [])

        widget_list = fw([
            QtWidgets.QAbstractSpinBox, QtWidgets.QAbstractButton,
            QtWidgets.QComboBox, QtWidgets.QAbstractSlider
        ])

        # operator list

        self.operatorList = action_list + widget_list + [self.ui.plotWidget.toolbar]

        self.deviceOperators = [
            self.ui.actionSpectrum_RTD, self.ui.actionAcquisition, self.ui.pushButtonBaselineAcq,
            self.ui.comboBoxITUnit, self.ui.spinBoxITTime, self.ui.spinBoxAccumulation
        ]

        self.fileOperators = [
            self.ui.actionExport, self.ui.actionSave_As, self.ui.actionFind_Peak,
            self.ui.checkBoxFixAxisRange, self.ui.plotWidget.toolbar, self.ui.actionOverlay
        ]

        self.exemptOperators = [
            self.ui.checkBoxFixAxisRange, self.ui.horizontalSliderYMax, self.ui.horizontalSliderYMin
        ]

        # finalize & set locks
        self.ui.groupBoxYAxisFixer.setEnabled_(False, "__rtd_exclusive")
        self.__update_data_acquired_status(False)
        self.ui.checkBoxSubtractBaseline.setEnabled_(False, "__accept_baseline")
        self.ui.checkBoxSubtractBaseline.setChecked(False)
        self.ui.pushButtonSaveBaseline.setEnabled_(False, "__accept_baseline")
        self.__check_driver()

    def __unit_toggled(self, checked, target):
        if self.units_lock:
            return
        self.units_lock = True
        # acquire operation lock
        if not checked:
            assert target == self.units
            self.unitActions[target].setChecked(True)
        else:
            if self.a1 is None or self.a2 is None:
                if self.__calibration() == 0:  # ok not checked
                    self.unitActions[self.units].setChecked(True)
                    self.unitActions[target].setChecked(False)
                    self.units_lock = False
                    return
            self.unitActions[self.units].setChecked(False)
            self.units = target
            if self.data is not None:
                self.plot()
        self.units_lock = False

    def __y_axis_fixing_value_changed(self, is_min_slider):
        min_ = self.ui.horizontalSliderYMin.value()
        max_ = self.ui.horizontalSliderYMax.value()
        if min_ >= max_:
            if is_min_slider:
                if min_ == self.ui.horizontalSliderYMin.maximum():
                    min_ = min_ - 1
                else:
                    max_ = max_ + 1
            else:
                if max_ == self.ui.horizontalSliderYMax.minimum():
                    max_ = max_ + 1
                else:
                    min_ = min_ - 1
            self.ui.horizontalSliderYMax.setValue(max_)
            self.ui.horizontalSliderYMin.setValue(min_)
        self.ui.horizontalSliderYMin.setToolTip(str(min_ * 1000))
        self.ui.horizontalSliderYMax.setToolTip(str(max_ * 1000))
        self.yMin, self.yMax = min_ * 1000, max_ * 1000
        self.plot()

    def __y_axis_fixing_toggled(self, checked):
        self.ui.horizontalSliderYMax.setEnabled_(checked, "__rtd_exclusive")
        self.ui.horizontalSliderYMin.setEnabled_(checked, "__rtd_exclusive")
        self.plot()

    def __integration_time_changed(self):
        it_threshold = 60  # unit: seconds
        if self.ui.comboBoxITUnit.currentIndex() == 1 and self.ui.spinBoxITTime.value() > it_threshold:
            self.ui.spinBoxITTime.setValue(it_threshold)


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    app.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps)
    mainWindow = MainWindow()
    mainWindow.show()
    mainWindow.on_start()
    sys.exit(app.exec_())
