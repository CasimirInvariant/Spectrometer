
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog

from calibrationUi import Ui_CalibrationDialog


class CalibrationDialog(QDialog):

    def __init__(self, parent, a1, a2, laser):
        QDialog.__init__(self, parent)
        self.setModal(True)
        self.setWindowFlags(self.windowFlags() & (~Qt.WindowContextHelpButtonHint) | Qt.MSWindowsFixedSizeDialogHint)
        self.ui = Ui_CalibrationDialog()
        self.ui.setupUi(self)

        self.ui.pushButtonOK.clicked.connect(self.accept)
        self.ui.pushButtonCancel.clicked.connect(self.reject)
        if a1 is not None and a2 is not None:
            self.ui.doubleSpinBoxCaliA1.setValue(a1)
            self.ui.doubleSpinBoxCaliA2.setValue(a2)
        self.ui.doubleSpinBoxLaserWL.setValue(laser)

    def ret(self):
        return [
            self.ui.doubleSpinBoxCaliA1.value(), self.ui.doubleSpinBoxCaliA2.value(),
            self.ui.doubleSpinBoxLaserWL.value()
        ]

    @staticmethod
    def get_calibration_params(parent, a1, a2, laser):
        dlg = CalibrationDialog(parent, a1, a2, laser)
        ok = dlg.exec_()
        ret = dlg.ret()
        return (*ret, ok)
